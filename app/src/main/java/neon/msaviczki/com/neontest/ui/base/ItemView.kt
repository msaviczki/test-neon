package neon.msaviczki.com.neontest.ui.base

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

interface ItemView {
    val type: Int
}