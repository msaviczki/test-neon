package neon.msaviczki.com.neontest.ui.home.model

import neon.msaviczki.com.neontest.ui.base.ItemView

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
class HistoricModel(override val type : Int = 0) : ItemView {

    constructor(score : Int, imageUrl : String, name : String) : this(){
        this.score=  score
        this.imageUrl = imageUrl
        this.name = name
    }

    var score = 0
    var imageUrl = ""
    var name = ""
}