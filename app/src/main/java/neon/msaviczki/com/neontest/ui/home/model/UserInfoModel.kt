package neon.msaviczki.com.neontest.ui.home.model

import neon.msaviczki.com.neontest.ui.base.ItemView

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class UserInfoModel(override val type : Int = 0) : ItemView {

    constructor(id : String, name : String, phone : String, url : String) : this(){
        this.id = id
        this.name = name
        this.phone = phone
        this.url = url
    }

    var id : String = ""
    var name : String = ""
    var phone : String = ""
    var url : String = ""
}