package neon.msaviczki.com.neontest.ui.home.contract

import neon.msaviczki.com.neontest.ui.base.Presenter
import neon.msaviczki.com.neontest.ui.base.View
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

interface SendMoneyContract{

    interface SendMoneyView : View{
        fun showUserInfo(info : MutableList<UserInfoModel>)
    }

    interface SendMoneyPresenter : Presenter{
        fun validateUserInfo(id : String)
        fun getUsers()
    }

}