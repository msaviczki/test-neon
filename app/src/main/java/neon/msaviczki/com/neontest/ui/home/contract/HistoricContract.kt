package neon.msaviczki.com.neontest.ui.home.contract

import neon.msaviczki.com.neontest.ui.base.Presenter
import neon.msaviczki.com.neontest.ui.base.View
import neon.msaviczki.com.neontest.ui.home.model.HistoricModel
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
interface HistoricContract {
    interface HistoricView : View{
        fun showHistoricResponse(user : MutableList<UserInfoModel>, historic : MutableList<HistoricModel>, topScore : Int)
    }

    interface HistoricPresenter : Presenter {
        fun requestHistoric()
    }
}