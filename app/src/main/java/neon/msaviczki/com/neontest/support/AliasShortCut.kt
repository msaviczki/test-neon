package neon.msaviczki.com.neontest.support

import neon.msaviczki.com.neontest.R


/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

typealias S = R.string
typealias C = R.color
typealias D = R.drawable
typealias L = R.layout
typealias A = R.anim
typealias I = R.id

