package neon.msaviczki.com.neontest.support

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet.view.*

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.enable() {
    isEnabled = true
}

fun View.disable() {
    isEnabled = false
}

infix fun ViewGroup.inflate(layoutResId: Int): View =
    LayoutInflater.from(context).inflate(layoutResId, this, false)

fun BottomSheetBehavior<View>.show() {
    state = BottomSheetBehavior.STATE_EXPANDED
}

fun BottomSheetBehavior<View>.hide() {
    state = BottomSheetBehavior.STATE_HIDDEN
}

fun BottomSheetBehavior<View>.buildBottomSheet(context: Context, bottomSheet: View, text : String) {
    with(bottomSheet) {
        toastMessage.text = text
        setOnClickListener { this@buildBottomSheet.hide() }
        postDelayed({
            this@buildBottomSheet.hide()
        }, 2000)
    }
}

