package neon.msaviczki.com.neontest.support

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import java.text.NumberFormat
import java.util.*
import android.util.DisplayMetrics



/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, action: (t: T) -> Unit): LiveData<T> =
    liveData.apply { observe(this@observe, Observer { observable -> observable?.let { action(it) } }) }

fun glideCircleLoading(view : ImageView, url : String){
    var options = RequestOptions()
    options = options.transform(CircleCrop())
    Glide.with(view.context)
        .load(url)
        .apply(options)
        .into(view)
}

fun String.getFirstLetterSplitted() : String{
    val split = this.split(" ")
    if(split.size > 1){
        val first = split[0]
        val second = split[1]
        val builder = StringBuilder()
        builder.append(first[0].toUpperCase())
        builder.append(second[0].toUpperCase())
        return builder.toString()
    } else {
        return this[0].toUpperCase().toString()
    }
}

fun spaceMoney(s: String, money: String) : String{
    return s.replace(money, "$money ")
}

fun pxToDp(px: Float, context: Context) : Int {
    return (px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}