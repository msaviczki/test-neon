package neon.msaviczki.com.neontest.support

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

fun AppCompatActivity.setTransparentStatusBar() {
    window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window.statusBarColor = Color.TRANSPARENT
}

inline fun <reified T> AppCompatActivity.start() {
    this.startActivity(Intent(this, T::class.java))
}