package neon.msaviczki.com.neontest.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        init()
    }

    protected abstract val layout : Int
    protected abstract fun init()
}