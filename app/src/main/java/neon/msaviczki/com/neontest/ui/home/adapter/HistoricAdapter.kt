package neon.msaviczki.com.neontest.ui.home.adapter

import android.view.ViewGroup
import neon.msaviczki.com.neontest.support.L
import neon.msaviczki.com.neontest.support.inflate
import neon.msaviczki.com.neontest.ui.base.BaseListAdapter
import neon.msaviczki.com.neontest.ui.base.ItemView

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
class HistoricAdapter(private val heigth : Int, private val maxScore : Int) : BaseListAdapter() {

    override fun getItemViewHolder(parent: ViewGroup): BaseViewHolder<ItemView> {
        return HistoricViewHolder(parent.inflate(L.history_item), heigth, maxScore) as BaseViewHolder<ItemView>
    }
}