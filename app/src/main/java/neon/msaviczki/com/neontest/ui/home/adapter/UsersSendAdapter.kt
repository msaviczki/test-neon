package neon.msaviczki.com.neontest.ui.home.adapter

import android.view.ViewGroup
import neon.msaviczki.com.neontest.support.L
import neon.msaviczki.com.neontest.support.inflate
import neon.msaviczki.com.neontest.ui.base.BaseListAdapter
import neon.msaviczki.com.neontest.ui.base.ItemView

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class UsersSendAdapter(private val listener : UsersSendViewHolder.UserInfoListener) : BaseListAdapter() {
    override fun getItemViewHolder(parent: ViewGroup): BaseViewHolder<ItemView> {
        return UsersSendViewHolder(parent.inflate(L.user_item), listener) as BaseViewHolder<ItemView>
    }
}