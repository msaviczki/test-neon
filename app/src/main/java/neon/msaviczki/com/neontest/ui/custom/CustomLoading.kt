package neon.msaviczki.com.neontest.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import neon.msaviczki.com.neontest.support.L
import neon.msaviczki.com.neontest.support.inflate

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class CustomLoading : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        addView(inflate(L.loading))
    }
}
