package neon.msaviczki.com.neontest.ui.home.presenter

import neon.msaviczki.com.neontest.ui.home.contract.SendMoneyContract
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class SendMoneyPresenter(private val sendMoneyView : SendMoneyContract.SendMoneyView)
    : SendMoneyContract.SendMoneyPresenter {

    override fun getUsers() {
        val userList : MutableList<UserInfoModel> = mutableListOf()
        userList.add(UserInfoModel("1", "Matheus Saviczki", "(11) 99883-9701", "http://images2.wikia.nocookie.net/__cb20120721124739/tudosobrehoradeaventura/pt-br/images/9/99/AT_Icons_100x100_Jake.jpg"))
        userList.add(UserInfoModel("2", "Paulo Roberto", "(11) 92383-7501", "http://www.avatarsdb.com/avatars/spongebob_happy.jpg"))
        userList.add(UserInfoModel("3", "Igor Santana", "(11) 99888-0001", ""))
        userList.add(UserInfoModel("4", "Felipe Oliveira", "(11) 96883-2101", ""))
        userList.add(UserInfoModel("5", "Leo Stronda", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("6", "Xunda", "(11) 99883-9701", ""))
        userList.add(UserInfoModel("7", "Teste teste", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("8", "Rafaela Silva", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("9", "Natila Coelho", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("10", "Thor Odinson", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("11", "Tony Stark", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("12", "Rick Morty", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("13", "Bruno Silva", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("14", "Cabo Daciolo", "(11) 99883-9701", "http://www.czicq.cz/files/avatary/the-simpsons-homer-thinking-100x100.png"))
        userList.add(UserInfoModel("15", "Jair Bolsonaro", "(11) 99883-9701", "https://vignette.wikia.nocookie.net/thehungergames/images/9/95/100x100.png"))
        val sorted = userList.sortedWith(compareBy { it.name })

        sendMoneyView.showUserInfo(sorted.toMutableList())
    }

    override fun validateUserInfo(id : String) {

    }

}