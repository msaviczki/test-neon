package neon.msaviczki.com.neontest.ui.home.view

import android.view.ViewTreeObserver
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_historic.*
import neon.msaviczki.com.neontest.support.L
import neon.msaviczki.com.neontest.support.pxToDp
import neon.msaviczki.com.neontest.support.setTransparentStatusBar
import neon.msaviczki.com.neontest.ui.base.BaseActivity
import neon.msaviczki.com.neontest.ui.home.adapter.HistoricAdapter
import neon.msaviczki.com.neontest.ui.home.adapter.UsersSendAdapter
import neon.msaviczki.com.neontest.ui.home.adapter.UsersSendViewHolder
import neon.msaviczki.com.neontest.ui.home.contract.HistoricContract
import neon.msaviczki.com.neontest.ui.home.model.HistoricModel
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel
import neon.msaviczki.com.neontest.ui.home.presenter.HistoricPresenter

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
class HistoricActivity : BaseActivity(), HistoricContract.HistoricView, UsersSendViewHolder.UserInfoListener {

    private val historicPresenter = HistoricPresenter(this)

    override val layout: Int = L.activity_historic

    override fun init() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setTransparentStatusBar()
        historicPresenter.requestHistoric()
    }

    override fun showHistoricResponse(user: MutableList<UserInfoModel>, historic: MutableList<HistoricModel>, topScore: Int) {
        val userAdapter = UsersSendAdapter(this)

        recyclerHistoric.layoutManager = LinearLayoutManager(this ,
            LinearLayoutManager.HORIZONTAL, false)
        recycler.layoutManager = LinearLayoutManager(this)
        userAdapter.addItems(user)

        recyclerHistoric.viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    recyclerHistoric.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val historicAdapter = HistoricAdapter(recyclerHistoric.height - 250, topScore)
                    historicAdapter.addItems(historic)
                    recyclerHistoric.adapter = historicAdapter
                }
            })

        recycler.adapter = userAdapter
    }

    override fun userInfoOnClick(item: UserInfoModel) {
        //NOTHING
    }
}