package neon.msaviczki.com.neontest.ui.home.view

import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_send.*
import neon.msaviczki.com.neontest.support.*
import neon.msaviczki.com.neontest.ui.base.BaseActivity
import neon.msaviczki.com.neontest.ui.custom.CustomDialog
import neon.msaviczki.com.neontest.ui.home.adapter.UsersSendAdapter
import neon.msaviczki.com.neontest.ui.home.adapter.UsersSendViewHolder
import neon.msaviczki.com.neontest.ui.home.contract.SendMoneyContract
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel
import neon.msaviczki.com.neontest.ui.home.presenter.SendMoneyPresenter

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class SendMoneyActivity : BaseActivity(),
    SendMoneyContract.SendMoneyView,
    UsersSendViewHolder.UserInfoListener,
    CustomDialog.SetNameListener{

    private val sendMoneyPresenter = SendMoneyPresenter(this)
    private lateinit var bottomSheetInfo : View

    private val toastBarInfo by lazy {
        BottomSheetBehavior.from(bottomSheetInfo)
    }

    override val layout: Int = L.activity_send
    override fun init() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setTransparentStatusBar()
        bottomSheetInfo = bottomSheet
        toastBarInfo.hide()
        sendMoneyPresenter.getUsers()
    }

    override fun showUserInfo(info: MutableList<UserInfoModel>) {
        val adapter = UsersSendAdapter(this)
        val animation : LayoutAnimationController = AnimationUtils.loadLayoutAnimation(this, A.anim_fall)

        adapter.addItems(info)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
        recycler.layoutAnimation = animation
        recycler.scheduleLayoutAnimation()
    }

    override fun userInfoOnClick(item: UserInfoModel) {
        val dialog = CustomDialog(this, item, this)
        dialog.show()
    }

    override fun userNameValidate(name: String, money : String) {
        val text = "Enviado $money para $name"
        toastBarInfo.show()
        toastBarInfo.buildBottomSheet(this, bottomSheetInfo, text)
    }
}