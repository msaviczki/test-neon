package neon.msaviczki.com.neontest.ui.home.view

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.button.view.*
import neon.msaviczki.com.neontest.support.*
import neon.msaviczki.com.neontest.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    override val layout: Int = L.activity_main

    override fun init() {
        setTransparentStatusBar()

        with(btnHistoric){
            txtBtn.text = context.getString(S.historic_send)
        }
        with(btnSendMoney){
            txtBtn.text = context.getString(S.send_money)
        }

        btnHistoric.setOnClickListener { start<HistoricActivity>() }
        btnSendMoney.setOnClickListener { start<SendMoneyActivity>() }
        glideCircleLoading(imgProfile, "https://www.skylightsearch.co.uk/wp-content/uploads/2017/01/Hayley-profile-pic-circle.png")

    }
}
