package neon.msaviczki.com.neontest.ui.home.adapter

import android.view.View
import kotlinx.android.synthetic.main.user_item.view.*
import neon.msaviczki.com.neontest.support.getFirstLetterSplitted
import neon.msaviczki.com.neontest.support.glideCircleLoading
import neon.msaviczki.com.neontest.support.hide
import neon.msaviczki.com.neontest.support.show
import neon.msaviczki.com.neontest.ui.base.BaseListAdapter
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */
class UsersSendViewHolder(itemView : View, private val listener : UserInfoListener) : BaseListAdapter.BaseViewHolder<UserInfoModel>(itemView){

    override fun bind(item: UserInfoModel) {
        with(itemView){
            if(item.url.isNotEmpty()){
                glideCircleLoading(imgProfile, item.url)
            } else {
                imgProfile.hide()
                txtInitials.show()
                txtInitials.text = item.name.getFirstLetterSplitted()
            }
            txtName.text = item.name
            txtNumber.text = item.phone
            itemView.setOnClickListener { listener.userInfoOnClick(item) }
        }
    }

    interface UserInfoListener{
        fun userInfoOnClick(item: UserInfoModel)
    }
}