package neon.msaviczki.com.neontest.ui.home.adapter

import kotlinx.android.synthetic.main.history_item.view.*
import android.view.View
import neon.msaviczki.com.neontest.support.getFirstLetterSplitted
import neon.msaviczki.com.neontest.support.glideCircleLoading
import neon.msaviczki.com.neontest.support.hide
import neon.msaviczki.com.neontest.support.show
import neon.msaviczki.com.neontest.ui.base.BaseListAdapter
import neon.msaviczki.com.neontest.ui.home.model.HistoricModel

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
class HistoricViewHolder(itemView : View, private val maxHeigth : Int, private val maxScore : Int) : BaseListAdapter.BaseViewHolder<HistoricModel>(itemView) {

    override fun bind(item: HistoricModel) {
        with(itemView){
            if(item.imageUrl.isNotEmpty()){
                glideCircleLoading(imgProfile, item.imageUrl)
            } else {
                imgProfile.hide()
                txtInitials.show()
                txtInitials.text = item.name.getFirstLetterSplitted()
            }
            txtScore.text = item.score.toString()

            val params = viewScore.layoutParams
            params.height = calculateHeigth(item.score)
            viewScore.layoutParams = params
        }
    }

    private fun calculateHeigth(score : Int) : Int{
        return (score * maxHeigth) / maxScore
    }
}