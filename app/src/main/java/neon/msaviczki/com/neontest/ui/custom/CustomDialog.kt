package neon.msaviczki.com.neontest.ui.custom

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.WindowManager
import kotlinx.android.synthetic.main.button.*
import kotlinx.android.synthetic.main.custom_dialog.*
import neon.msaviczki.com.neontest.R
import neon.msaviczki.com.neontest.support.*
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel
import java.text.NumberFormat
import java.util.*

/**
 * Created by Matheus Saviczki on 07-Janeiro-2019
 */

class CustomDialog(context : Context,
                   private val model : UserInfoModel,
                   private val listener : SetNameListener)
    : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_dialog)
        window.setBackgroundDrawable(null)
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        startUI()
    }

    private fun startUI(){
        close.setOnClickListener { dismiss() }
        txtName.text = model.name
        txtPhone.text = model.phone
        if(model.url.isNotEmpty()){
            glideCircleLoading(imgProfile, model.url)
        } else {
            imgProfile.hide()
            txtInitials.show()
            txtInitials.text = model.name.getFirstLetterSplitted()
        }

        with(btnSend){
            txtBtn.text = "ENVIAR"
        }
        btnSend.setOnClickListener {
            btnSend.hide()
            loading.show()
            editDialog.disable()
            close.disable()
            Handler().postDelayed({
                dismiss()
                listener.userNameValidate(model.name, editDialog.text.toString())
            }, DELAY)
        }
        editDialog.inputType = InputType.TYPE_CLASS_NUMBER
        editDialog.filters = arrayOf<InputFilter>(
            InputFilter.
                LengthFilter(17))
        editDialog.addTextChangedListener(object : TextWatcher{
            var old = ""
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s.toString() != old && !s.toString().isEmpty()){
                    editDialog.removeTextChangedListener(this)
                    val cleanString = s.toString().replace("[R$,. ]".toRegex(), "").trim()
                    old = cleanString
                    if(cleanString.isNotEmpty()){
                        val parsed = java.lang.Double.parseDouble(cleanString)
                        val formatted = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("pt-BR")).format((parsed/100))
                        old = formatted
                        old = spaceMoney(old, "R$")
                    }
                    editDialog.setText(old)
                    editDialog.setSelection(old.length)
                    editDialog.addTextChangedListener(this)
                }
            }
        })
    }

    companion object {
        const val DELAY : Long = 3000
    }

    interface SetNameListener{
        fun userNameValidate(name : String, money : String)
    }
}