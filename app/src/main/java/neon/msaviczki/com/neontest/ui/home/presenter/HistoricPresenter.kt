package neon.msaviczki.com.neontest.ui.home.presenter

import neon.msaviczki.com.neontest.ui.home.contract.HistoricContract
import neon.msaviczki.com.neontest.ui.home.model.HistoricModel
import neon.msaviczki.com.neontest.ui.home.model.UserInfoModel

/**
 * Created by Matheus Saviczki on 10-Janeiro-2019
 */
class HistoricPresenter(private val historicView : HistoricContract.HistoricView) : HistoricContract.HistoricPresenter {

    override fun requestHistoric() {
        val userList : MutableList<UserInfoModel> = mutableListOf()
        userList.add(UserInfoModel("1", "Matheus Saviczki", "(11) 99883-9701", "http://images2.wikia.nocookie.net/__cb20120721124739/tudosobrehoradeaventura/pt-br/images/9/99/AT_Icons_100x100_Jake.jpg"))
        userList.add(UserInfoModel("2", "Paulo Roberto", "(11) 92383-7501", "http://www.avatarsdb.com/avatars/spongebob_happy.jpg"))
        userList.add(UserInfoModel("3", "Igor Santana", "(11) 99888-0001", ""))
        userList.add(UserInfoModel("4", "Felipe Oliveira", "(11) 96883-2101", ""))
        userList.add(UserInfoModel("5", "Felipe Oliveira", "(11) 96883-2101", ""))
        val sorted = userList.sortedWith(compareBy { it.name })

        val historicList : MutableList<HistoricModel> = mutableListOf()
        historicList.add(HistoricModel(1200, "http://images2.wikia.nocookie.net/__cb20120721124739/tudosobrehoradeaventura/pt-br/images/9/99/AT_Icons_100x100_Jake.jpg", "Matheus Saviczki"))
        historicList.add(HistoricModel(1000, "http://www.avatarsdb.com/avatars/spongebob_happy.jpg", "Paulo Roberto"))
        historicList.add(HistoricModel(800, "", "Igor Santana"))
        historicList.add(HistoricModel(600, "", "Felipe Oliveira"))
        historicList.add(HistoricModel(400, "", "Felipe Oliveira"))

        val maxScore = historicList.maxBy { it.score }

        historicView.showHistoricResponse(sorted.toMutableList(), historicList, maxScore!!.score)
    }
}